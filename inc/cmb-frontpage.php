<?php


/**
 * Campos para la paginade inicio (Front page)
 */


//Campos de prueba seccion 1
add_action( 'cmb2_admin_init', '_s_section1' );
function _s_section1() {
    $prefix = 'fp_section_1';
    $id_home = get_option( 'page_on_front' );

    $_s_frontPage = new_cmb2_box( array(
        'id'            => $prefix.'titleSection',
        'title'         => esc_html__( 'Ajustes: seccion 1', 'cmb2' ),
        'object_types'   => array ( 'page' ), //Post type
        'context'       => 'normal',
        'priotiry'      => 'high',
        'show_names'    => true,
        'show_on'        => array( 
            'id' => ($id_home)
        )
    ) );

    $_s_frontPage->add_field( array(
		'id'         => $prefix.'campo1',
		'name'       => esc_html__( 'Texto inicio 1', 'cmb2' ),
		'desc'       => esc_html__( 'Hola mundo esta es una descripción', 'cmb2' ),
		'type'       => 'text',
    ) );
    
    $_s_frontPage->add_field( array(
		'id'         => $prefix.'campo2',
		'name'       => esc_html__( 'Texto inicio 2', 'cmb2' ),
		'desc'       => esc_html__( 'Hola mundo esta es una descripción', 'cmb2' ),
		'type'       => 'text',
	) );
}

add_action( 'cmb2_admin_init', '_s_section2' );
function _s_section2() {
    $prefix = 'fp_section_2';
    $id_home = get_option( 'page_on_front' );

    $_s_frontPage = new_cmb2_box( array(

        'id'            => $prefix.'titleSection',
        'title'         => esc_html__( 'Ajustes: seccion 2', 'cmb2' ),
        'object_types'   => array ( 'page' ), //Post type
        'context'       => 'normal',
        'priotiry'      => 'high',
        'show_names'    => true,
        'show_on'        => array( 
            'id' => ($id_home)
        )

    ) );

    $_s_frontPage->add_field( array(
		'id'         => $prefix.'campo1',
		'name'       => esc_html__( 'Texto inicio 1', 'cmb2' ),
		'desc'       => esc_html__( 'Hola mundo esta es una descripción', 'cmb2' ),
		'type'       => 'text',
    ) );
    
    $_s_frontPage->add_field( array(
		'id'         => $prefix.'campo2',
		'name'       => esc_html__( 'Texto inicio 2', 'cmb2' ),
		'desc'       => esc_html__( 'Hola mundo esta es una descripción', 'cmb2' ),
		'type'       => 'text',
	) );

}